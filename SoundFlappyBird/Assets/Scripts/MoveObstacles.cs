﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObstacles : MonoBehaviour {

    public Vector2 vel = new Vector2(-4, 0);
    Rigidbody2D rig;
	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody2D>();

        rig.velocity = vel;
        transform.position = new Vector3(transform.position.x, transform.position.y - 3 * Random.value, transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if(this.transform.position.x < -12)
        {
            IncreaseScore();
            if(GenerateObstacles.score > GenerateObstacles.highScore)
            {
                IncreaseHighscore(GenerateObstacles.score);
            }
            Destroy(this);
        }
	}

    public void IncreaseScore()
    {
        GenerateObstacles.score++;
    }

    public void IncreaseHighscore(int score)
    {
        GenerateObstacles.highScore = GenerateObstacles.score;
    }
}
