﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObstacles : MonoBehaviour {

    public GameObject obstacle;
    public static int score = 0;
    public static int highScore = 0;
    // Use this for initialization
    Movement player;
    MoveObstacles collumn;
	void Start () {
        InvokeRepeating("Obstacle", 2, 2);
        player = GetComponent<Movement>();
        collumn = GetComponent<MoveObstacles>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnGUI()
    {
        GUI.color = Color.white;
        GUILayout.Label("Score: " + score.ToString()+ "\nAll Time:" + highScore.ToString());
    }
    void Obstacle()
    {
        Instantiate(obstacle);       
    }
}
