﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAnalysis : MonoBehaviour {

    AudioSource source;
    public FFTWindow window;
    public static float[] samples = new float[64];
    public float volume = 0;
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
        GetSpectrumSource();
        
	}
    void GetSpectrumSource()
    {
        source.GetSpectrumData(samples, 0, window);
        AvgVolume();
    }
    public float AvgVolume()
    {
        float a = 0;

        foreach (float s in samples)
        {
            a += Mathf.Abs(s);
        }

        return a / samples.Length;
    }
}
