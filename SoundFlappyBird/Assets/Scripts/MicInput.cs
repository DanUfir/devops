﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicInput : MonoBehaviour {

    int audioSampleRate = 44100;
    string mic;
    AudioSource source;
    List<string> audioSources = new List<string>();
    float[] samples = new float[64];
    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();

        foreach (string device in Microphone.devices)
        {
            if (mic == null)
            {
                mic = device; // make default device equal to the mic

            }
            //            audioSources.add(device);
        }
        UpdateMic();
    }

   
    void UpdateMic()
    {
        source.Stop();

        source.clip = Microphone.Start(mic, true, 5, audioSampleRate);
        source.loop = true;

        if (Microphone.IsRecording(mic))
        {
            while (!(Microphone.GetPosition(mic) > 0))
            { }

            source.Play();
        }
        else
        {
            Debug.Log("Mic not working");
        }
    }
}
